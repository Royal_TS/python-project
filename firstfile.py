##----Global Variables----#

# Game board
board = ["-", "-","-",
         "-", "-", "-",
         "-", "-", "-",]

# If game is still going
game_still_going = True

# Who won? Or tie?
winner = None

# Whos turn is it
current_player = "X"

# Play again?
question = ""
List_of_answer_no = ['Nah', 'nah', 'Nope', 'nope', 'no way', 'No way',
                     'Nein', 'nein', 'No', 'no', 'nada', 'Nada', 'no',
                     'Nein Danke', 'nein danke']

List_of_answer_yes = ['Ja', 'ja', 'Yes', 'yes', 'absulutely', 'sowas von',
                      'Sowas von', 'Yeah', 'yeah', 'of course']


# Display board
def display_board():
    print("|" + board[0] + "|" + board[1] + "|" + board[2] + "|")
    print("|" + board[3] + "|" + board[4] + "|" + board[5] + "|")
    print("|" + board[6] + "|" + board[7] + "|" + board[8] + "|")

# Start the game
def play_game():
    # Set up global variables
    global game_still_going
    global current_player
    
    # Display initial board
    display_board()

    # While the game is still going
    while game_still_going == True:
    
        #handle a single turn of an arbitrary player	
        handle_turn(current_player)

        # Check if the game has ended
        check_if_game_over()

        # Flip to the other Player
        flip_player()

        # The game has ended
    if winner == "X" or winner == "O":
        print(winner + " hat gewonnen!")
        game_still_going = False
    elif winner == None:
        print("Unentschieden")
        game_still_going = False
    
        # Asks if you want wo play again    
    if game_still_going == False:
        # Set up global variables
        global question
        global board
    
        question = input('Noch eine Runde? Ja/Nein: ')
    
        if question in List_of_answer_yes:
            Start_over()
        elif question in List_of_answer_no:
            print('Ok langweiler') 
    return
        
    # Handle a single turn of an arbitrary player
def handle_turn(player):

    print(player + " ist drann.")
    position = input("Wähle einen Platz von 1-9: ")

    valid = False
    while not valid:

        while position not in ['1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9']:
            position = input("Du Idiot, es geht nur von 1-9: ")

        position = int(position) - 1

        if board[position] == '-':
            valid = True
        else:
            print("Da steht doch schon was ( ͡ಠ ʖ̯ ͡ಠ )")
            position = input("Wähle einen Platz von 1-9:")
    

    board[position] = player

    display_board()
     
    # Checks if there is a winner or it's a tie
def check_if_game_over():
    check_for_winner()
    check_if_tie()

    # Checks if there is a winner
def check_for_winner():

    # Set up global variables
    global winner

    # check rows
    row_winner = check_rows()
    
    # check columns
    column_winner = check_columns()
    
    # check diagonals
    diagonal_winner = check_diagonals()
    if row_winner:
        winner = row_winner
    elif column_winner:
        winner = column_winner
    elif diagonal_winner:
        winner = diagonal_winner
    else: 
        winner = None
    return

    # Checks if there is a winner on a row
def check_rows():
    # Set up global variables
    global game_still_going

    # check if any of the rows have all the same value (and is not empty)
    row_1 = board[0] == board[1] == board[2] != '-'
    row_2 = board[3] == board[4] == board[5] != '-'
    row_3 = board[6] == board[7] == board[8] != '-'

    # If any row does have a match, flag that there is a win
    if row_1 or row_2 or row_3:
        game_still_going = False

    # return the winner (X or 0)
    if row_1:
        return board[0]
    elif row_2:
        return board[3]
    elif row_3:
        return board[6]
    return

    # Checks if there is a winner on a column
def check_columns():
    # Set up global variables
    global game_still_going

    # check if any of the column have all the same value (and is not empty)
    column_1 = board[0] == board[3] == board[6] != '-'
    column_2 = board[1] == board[4] == board[7] != '-'
    column_3 = board[2] == board[5] == board[8] != '-'

    # If any column does have a match, flag that there is a win
    if column_1 or column_2 or column_3:
        game_still_going = False

    # return the winner (X or 0)
    if column_1:
        return board[0]
    elif column_2:
        return board[1]
    elif column_3:
        return board[2]
    return    


    # Checks if there is a winner on a diagonal
def check_diagonals():
    # Set up global variables
    global game_still_going

    # check if any of the diagonals have all the same value (and is not empty)
    diagonals_1 = board[0] == board[4] == board[8] != '-'
    diagonals_2 = board[6] == board[4] == board[2] != '-'

    # If any diagonals does have a match, flag that there is a win
    if diagonals_1 or diagonals_2:
        game_still_going = False

    # return the winner (X or 0)
    if diagonals_1:
        return board[0]
    elif diagonals_2:
        return board[6]
    return

    # Checks if the board is full and if there is no winner
def check_if_tie():
    global game_still_going

    # Checks if there a still free places on the board
    if '-' not in board:
        game_still_going = False
    return

    # Change the Player after valid input
def flip_player():
    # gloabal variable we need
    global current_player

    # if current player was X, then change it to O
    if current_player == 'X':
     current_player = 'O'

     # if current player was O, then change it to X
    elif current_player == 'O':
        current_player = 'X'
    return

# if Player want to play again start over
def Start_over():
    # set global variable
    global board
    global game_still_going
    global current_player
    
    board = ["-", "-","-",
             "-", "-", "-",
             "-", "-", "-",]
    game_still_going = True
    current_player = 'X'
    play_game()     

play_game()